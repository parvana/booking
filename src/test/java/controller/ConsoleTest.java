package controller;

import org.junit.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class ConsoleTest {


    @Test
    public void flightId() throws IOException {
        FlightController fc= new FlightController();
        String id="682845";
        assertEquals("682845/06:58/Baku/2019-11-13/73/",fc.get(id));
    }
    @Test
    public void flightId2() throws IOException {
        FlightController fc= new FlightController();
        String id="682845fghj";
        assertEquals("There is no such a flight",fc.get(id));
    }

    @Test
    public void searchAndBook() throws IOException {
        FlightController fc = new FlightController();
        BookingController bookingController=new BookingController();
        String destionation="Ankara";
        String date="2019-10-10";
        int countofPeople=2;
        assertEquals(new ArrayList<>(Arrays.asList("There is not such a flight")),fc.search(destionation,date,countofPeople));

    }
    @Test
    public void searchAndBook2() throws IOException {
        FlightController fc = new FlightController();
        BookingController bookingController=new BookingController();
        String destionation="Ankara";
        String date="2019-11-14";
        int countofPeople=2;
        assertEquals(new ArrayList<>(Arrays.asList("405033/05:58/Ankara/2019-11-14/48/")),fc.search(destionation,date,countofPeople));

    }
    @Test
    public void wantToBook() {
    }

    @Test
    public void cancelBookingTest1() throws IOException {
        BookingController bookingController = new BookingController();
        String id="1145";
        assertTrue(bookingController.update(id));
    }
    @Test
    public void cancelBookingTest2() throws IOException {
        BookingController bookingController = new BookingController();
        String id="Unknown Anonym";
        assertFalse(bookingController.update(id));
    }

    @Test
    public void showMyFlightTest1() throws IOException {
        BookingController bookingController = new BookingController();
        String name="sdf d";
        assertEquals("[1145/405033/sdf d/]",bookingController.getAllMyFlights(name));

    }
    @Test
    public void showMyFlightTest2() throws IOException {
        BookingController bookingController = new BookingController();
        String name="No One";
        assertEquals(null,bookingController.getAllMyFlights(name));

    }
}
