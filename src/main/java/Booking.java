public class Booking {
  private   String bid;
  private Passengers[] passengers;
  private String flightid;


    public Booking() {
    }
    public Booking(String bid, Passengers[] passengers, String flightid) {
        this.bid = bid;
        this.passengers = passengers;
        this.flightid = flightid;
    }

    public String getBid() {
        return bid;
    }

    public Passengers[] getPassengers() {
        return passengers;
    }

    public String getFlightid() {
        return flightid;
    }

    public void setFlightid(String flightid) {
        this.flightid = flightid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }
    public void setPassengers(Passengers[] passengers) {
        this.passengers = passengers;
    }


}
