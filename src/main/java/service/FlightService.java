package service;

import DAO.DAO;
import DAO.FlightDao;
import Flight;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FlightService {
private DAO<Flight> dao ;


 public FlightService() throws IOException
  {
      dao = new FlightDao();
  }

  public void store(Flight data){
        dao.store(data);
}

  public void close() throws IOException{
        dao.close();
   }
  public ArrayList<String> getAllFlights() throws IOException {
        return dao.getAllFlights();
    }

  public String get(String id){
        return dao.get(id);
    }


    public void delete(String id) {
     dao.delete(id);
    }

    public void update(Flight newflight) {
     dao.updateFlight(newflight);
    }

    public List<String> search(String destination, String date, int countOfPeople) throws IOException{
     return dao.search(destination,date,countOfPeople);}
}
