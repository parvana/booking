package service;

import DAO.BookingDao;
import DAO.DAO;
import Booking;

import java.io.IOException;
import java.util.ArrayList;

public class BookingService {
    private DAO<Booking> bookingDao;

    public BookingService() throws IOException {
        bookingDao=new BookingDao();
    }

    public void store(Booking data){
        bookingDao.store(data);
    }

    public void delete(String id){
        bookingDao.delete(id);
    }

    public ArrayList<String> getAllMyFlights(String name) throws IOException {
        return  bookingDao.getAllMyFlights(name);
    }

    public void close() throws IOException{
        bookingDao.close();
    }


    public boolean update(String s) {
        return bookingDao.updateBooking(s);
    }

    public ArrayList<String> showMyFlights(String fullname) throws IOException {
        return bookingDao.getAllMyFlights(fullname);
    }
}
