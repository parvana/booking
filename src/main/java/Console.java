import controller.BookingController;
import controller.FlightController;

import java.io.IOException;
import java.util.*;

public class Console {

    private static List<String> listCategory=new ArrayList<String>(Arrays.asList("1.Online-board","2.Show the flight info.","3.Search and Book","4.Cancel the Booking","5.My flights","6.Exit"));


    public Console() throws IOException {
    }

    public static void showMenu(){
        for(String c:listCategory){
            System.out.println(c);
        }
    }
    public static int inputCategory() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter category:");
        try {
           int userChoice=Integer.parseInt(scan.next());
            if(userChoice<1 || userChoice>6){
                System.out.println("Please,enter number in the range of [1,6]");
                 return inputCategory();
            }
            else return userChoice;
            }
        catch (Exception e){
            System.out.println("You must choose category declaring number between 1 and 6");
            return inputCategory();
        }
    }
    public static void printAllFlights() throws IOException {
        FlightController fc = new FlightController();
        List<String> l=fc.getAllFlights();
        System.out.println("All flights from Kiev for the next 24 hours");
        l.stream().forEach(e->System.out.printf("Flight:%s%n ",e));

    }
    public static void flightId() throws IOException {
        FlightController fc= new FlightController();
        Scanner scan = new Scanner(System.in);
        System.out.println("Please, enter flight id: ");
        String id="";
        while (true){
            id =scan.nextLine();
            if(id.isEmpty()) System.out.println("Flight id should not be empty.Please, try again");
            else break;
        }
        System.out.println(fc.get(id));
    }
    public static void searchAndBook() throws IOException {
        FlightController fc = new FlightController();
        Scanner scan = new Scanner(System.in);
        String destination="";
        String date="";
        System.out.println("Please enter destination: ");

        while(true){
            destination=scan.nextLine();
            if(destination.isEmpty())  System.out.println("Input should not be empty,please try again");
            else break;
        }

        System.out.println("Please enter date in \"yyyy-mm-dd\" format");
        while (true){
            date=scan.nextLine();
            if(date.isEmpty())  System.out.println("Input should not be empty,please try again");
                else break;

        }
        System.out.println("Please enter count of people");
        int countOfPeople=0;
           String str="";
        while(true){
            try{
            str=scan.nextLine();
            countOfPeople=Integer.parseInt(str);
            break;}
            catch (NumberFormatException e){
                System.out.println("You should enter integer and input should not be empty,please try again:");
            }
        }

              List<String> searchedflights = fc.search(destination, date, countOfPeople);
              System.out.println(searchedflights.toString());
              if(searchedflights.get(0).equals("There is not such a flight")) return;
              System.out.println("Do you want to book yes/no");
              String answer = scan.next();
              if (answer.equals("yes")) {
                 wantToBook(countOfPeople);
              }

    }
    public static void wantToBook(int countOfPeople) throws IOException{
        FlightController fc = new FlightController();
        BookingController bookingController=new BookingController();
        Scanner scan = new Scanner(System.in);
            Passengers passengers[]=null;
            Booking booking=new Booking();
                try {
                    System.out.println("Please, enter flight id");
                    String chosenflightid="";
                    while (true){
                      chosenflightid = scan.nextLine();
                      if(chosenflightid.isEmpty()) System.out.println("Flight id should not be empty.Please, try again");
                        else break;
                    }
                    String chosenflight = fc.get(chosenflightid);
                        String[] cf = chosenflight.split("/");
                        //create flight
                        Flight newflight = new Flight();
                        newflight.setId(cf[0]);
                        newflight.setTime(cf[1]);
                        newflight.setDestination(cf[2]);
                        newflight.setDate(cf[3]);
                        newflight.setFreeSeats(Integer.parseInt(cf[4]) - countOfPeople);
                        //end of creation .now add this flight to flight.txt
                        fc.update(newflight);
                        System.out.println("enter passengers' name");
                        passengers = new Passengers[countOfPeople];
                        for (int i = 0; i < passengers.length; i++) {
                            System.out.println("enter " + (i + 1) + " passenger name");
                            String name="";
                            while(true){
                                name = scan.nextLine();
                                if(name.isEmpty()) System.out.println("Name should not be empty.Please, try again");
                                else break;
                            }
                            System.out.println("enter " + (i + 1) + "passenger surname");
                            String surname ="";
                            while (true){
                                surname = scan.nextLine();
                                if(surname.isEmpty()) System.out.println("Surname should not be empty.Please, try again");
                                else break;
                            }
                            Passengers p = new Passengers(name, surname);
                            passengers[i] = p;
                        }
                        Random r = new Random();
                        String bid = String.valueOf((r.nextInt(1001) + 1000));
                        booking.setBid(bid);
                        booking.setFlightid(chosenflightid);
                        booking.setPassengers(passengers);
                        bookingController.store(booking);
                        bookingController.close();
                    System.out.println("Booking completed");
                }
                catch (Exception e){
                    System.out.println("invalid flight id");
                    wantToBook(countOfPeople);
                }
    }

    public static void cancelBooking() throws IOException {
        Scanner scan = new Scanner(System.in);
        BookingController bookingController = new BookingController();
        System.out.println("Please enter your id in order to cancel ");
        String s;
        int num=0;
        while(true){
            try{
                s=scan.nextLine();
                num=Integer.parseInt(s);
                break;}
            catch (NumberFormatException e){
                System.out.println("You should enter integer and input should not be empty,please try again:");
            }

        }
        if(bookingController.update(s)==true)
        System.out.println("Your flights has been successfully cancelled");
        else System.out.println("There is no any relevant booking");
    }

    public static void showMyFlights() throws IOException {
        Scanner scan = new Scanner(System.in);
        BookingController bookingController = new BookingController();
        System.out.println("Please enter your full name: ");
        String fullname="";
            while(true) {
                fullname = scan.nextLine();
                if (fullname.isEmpty()) System.out.println("Input should not be empty,please try again");
                else break;
            }
        System.out.println("Your flights "+bookingController.showMyFlights(fullname));

    }
}


