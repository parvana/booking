package DAO;

import Flight;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class FlightDao implements DAO<Flight> {
    File f=new File("flights.txt");
    FileWriter fwriter =new FileWriter(f,true);
    BufferedWriter bufWriter=new BufferedWriter(fwriter);
    FileReader freader=new FileReader(f);
    BufferedReader bufReadeR=new BufferedReader(freader);
    ArrayList<Flight> flightList = new ArrayList<>();
    private   ArrayList<String> list;

    public FlightDao() throws IOException {
    }

    public void store(Flight data) {
        try {
            bufWriter.write(data.getId());
            bufWriter.write("/");
            bufWriter.write(data.getDestination());
            bufWriter.write("/");
            bufWriter.write(data.getDate());
            bufWriter.write("/");
            bufWriter.write(data.getTime());
            bufWriter.write("/");
            bufWriter.write(data.getFreeSeats());
            bufWriter.write("/");
            bufWriter.write("\n");

        } catch (IOException e) {
            e.printStackTrace();

        }

    }
    public String get(String id){
        try {
            String line=null;
            while((line=bufReadeR.readLine())!=null){
                String [] temp=line.split("/");
                if(temp[0].equals(id)){
                    return line;
                }
            }
            bufReadeR.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return "There is no such a flight";

    }

    @Override
    public ArrayList<String> getAllFlights() throws IOException {
        list= (ArrayList<String>) Files.readAllLines(Paths.get("flights.txt"));
        return list;

    }

    @Override
    public ArrayList<String> getAllMyFlights(String id) throws IOException {
        return null;
    }

    @Override
    public List<String> search(String destination, String date, int countOfPeople) throws IOException {
        String searchedFlight="/"+destination+"/"+date+"/";
        List<String> searchedflights=new ArrayList<>();
        for (String s: getAllFlights()) {
            if(s.contains(searchedFlight)){
                String [] flightarr=s.split("/");
                if(Integer.parseInt(flightarr[flightarr.length-1])>=countOfPeople){
                    searchedflights.add(s);
                }
            }
        }
        if(searchedflights.size()!=0)  return searchedflights;
         return new ArrayList<>(Arrays.asList("There is not such a flight"));
    }

    public void delete(String id) {

    }

    @Override
    public void update(Flight data) {

    }

    @Override
    public List<Flight> getAll() {
        return null;
    }

    @Override
    public List<Flight> getBy(Map<String, String> content) {
        return null;
    }

    public void updateFlight(Flight newFlight ) {
        try {
          List<String> file = new ArrayList<String>(Files.readAllLines(Paths.get("flights.txt"), StandardCharsets.UTF_8));
            for (int i = 0; i <file.size() ; i++) {
                if (file.get(i).contains(newFlight.getId())) {
                    file.set(i,
                            newFlight.getId() + "/" +
                                    newFlight.getTime() + "/" +
                                    newFlight.getDestination() + "/" +
                                    newFlight.getDate() + "/" +
                                    newFlight.getFreeSeats()+"/");
                    break;
                }
            }
            Files.write(Paths.get("flights.txt"),file,StandardCharsets.UTF_8);
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }

    @Override
    public boolean updateBooking(String id) {
        return false;
    }


}
