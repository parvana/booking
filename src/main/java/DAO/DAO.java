package DAO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface DAO <T>{
    void store(T data);
    String get(String id) ;
    void delete(String id);
    void update(T data);
//    void updateFlight(T data);
//    boolean updateBooking(String id);
//    void close() throws IOException;
    List<T> getAll();
//    ArrayList<String> getAllFlights() throws IOException;
//    ArrayList<String> getAllMyFlights(String name) throws IOException;
    List<T> getBy(Map<String,String> content);
//    List<String> search(String destination, String date, int countofPeople) throws IOException;

}


