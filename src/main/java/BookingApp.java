import controller.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class BookingApp {
    public static String createid(){
        Random r=new Random();
        int i=0;
        String id="";
        while(i<6){
            id=id+ r.nextInt(9);
            i++;
        }
        return id;
    }

    public static String createdate(){
        Random r=new Random();
        LocalDate l=LocalDate.now();
        return l.plusDays(r.nextInt(10)).toString();

    }
    public static String createtime(){
        Random r=new Random();
        LocalTime ltime=LocalTime.now();
        String time=ltime.plusHours(r.nextInt(24)).format(DateTimeFormatter.ofPattern("HH:mm"));
        return time;
    }
    public static int seats(){
        Random r=new Random();
        return r.nextInt(100);
    }

    public static void main(String[] args) throws IOException {
        FlightController flightcontroller=new FlightController();
        BookingController bookingController = new BookingController();
        List<String> l=new ArrayList<>(Arrays.asList("Budapest","Istanbul","Baku","Bucharest","Ankara","Tallinn","Moscow","Berlin","Warsaw","Milan"));
        int i=0;
        while(i<10){
            Flight f=new Flight(createid(),l.get(i),createdate(),createtime(),seats());
            flightcontroller.store(f);
            i++;
        }
        flightcontroller.close();
        boolean flag = true;
        while(flag){
            Console.showMenu();
            switch(Console.inputCategory()){
                case 1: Console.printAllFlights(); break;
                case 2: Console.flightId();break;
                case 3: Console.searchAndBook(); break;
                case 4: Console.cancelBooking();break;
                case 5: Console.showMyFlights(); break;
                case 6: flag=false; break;
            }

        }






    }

}
