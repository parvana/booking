package controller;

import service.BookingService;

import java.io.IOException;
import java.util.ArrayList;

public class BookingController {
   private BookingService bookingService;

    public BookingController() throws IOException {
        bookingService= new BookingService();
    }

    public void store(Booking data){
        bookingService.store(data);
    }
    public void delete(String id) {bookingService.delete(id);}
    public ArrayList<String> getAllMyFlights(String name) throws IOException {
        return bookingService.getAllMyFlights(name);
    }

    public void close() throws IOException {

        bookingService.close();
    }

    public boolean update(String s) {
       return bookingService.update(s);
    }

    public ArrayList<String> showMyFlights(String fullname) throws IOException {
      return  bookingService.showMyFlights(fullname);
    }
}
