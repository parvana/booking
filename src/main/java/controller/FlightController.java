package controller;
import service.FlightService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FlightController {
    private FlightService flightService;

    public FlightController() throws IOException {
        this.flightService=new FlightService();

    }

   public void store(Flight data){
        flightService.store(data);
   }
   public ArrayList<String> getAllFlights() throws IOException {
        return flightService.getAllFlights();
   }
   public String get(String id){
        return flightService.get(id);
   }
   public void close() throws IOException{
        flightService.close();
   }
   public void delete(String id){
        flightService.delete(id);
   }

    public void update(Flight newflight) {
        flightService.update(newflight);
    }

    public List<String> search(String destination, String date, int countOfPeople) throws IOException{
        return flightService.search(destination,date,countOfPeople);}
}
